/**
 * Created by Corey600 on 2016/6/15.
 */

'use strict';

// require core modules
var net = require('net');

// require thirdpart modules
var debug = require('debug');

var Proxy = require('hessian-proxy-xl').Proxy;
// print debug info
var error = debug('zoodubbo:error');

/**
 * Expose `Provider`.
 *
 * @type {Provider}
 */
module.exports = Provider;

/**
 * Constructor of Provider.
 *
 * @param {ZD} zd
 * @param {Object} opt
 * {
 *  path: String // The path of service node.
 *  version: String, // The version of service.
 *  timeout: Number, // Timeout in milliseconds, defaults to 60 seconds.
 * }
 * @returns {Provider}
 * @constructor
 */
function Provider(opt) {
    this.uri= opt.uri;
    this.host= opt.hostname;
    this.port= opt.port;
    this.methods= opt.methods;
    this.protocol= opt.protocol
}

/**
 * Excute the method
 *
 * @param {String} method
 * @param {Array} args
 * @param {Function} [cb]
 * @public
 */
Provider.prototype.invoke = function (method, args, callback) {
    var self = this;
    var hessianProxy = new Proxy(self.uri, '', '', hessianProxy);
    hessianProxy.invoke(method, args, callback);
};
